function each(inputArray, callBackFunction){
    if(!inputArray || inputArray.length == 0) return;
    //call function 'callBacks' and pases array element as parameter one by one.
    for(let arrIndex = 0; arrIndex < inputArray.length; arrIndex++){
        callBackFunction(inputArray[arrIndex], arrIndex, inputArray);
    }
}

module.exports = {each};