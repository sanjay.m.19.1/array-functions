function flatten(arr, depth = Infinity){
    if(!arr) return [];
    let outputArr = [];
    let currentDepth;
    for(let subArr = 0; subArr < arr.length; subArr++){
        currentDepth = depth;
        if(Array.isArray(arr[subArr]) == true && currentDepth){
            let temp = flatten(arr[subArr], --currentDepth);
            outputArr.push(...temp);
        } else {
            outputArr.push(arr[subArr]);
        }
    }
    return outputArr;
}

module.exports = {flatten};