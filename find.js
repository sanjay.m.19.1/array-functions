function find(inputArray, callBackFunction){
    //function only takes array as input and does not execute for empty array.
    if(!inputArray || inputArray.length == 0) return undefined;
    for(let arrayIndex = 0; arrayIndex < inputArray.length; arrayIndex++){
        if(callBackFunction(inputArray[arrayIndex])) return inputArray[arrayIndex];
    }
    return undefined;
}

module.exports = {find};