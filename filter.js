function filter(inputArray, callBackFunction){
    if(!inputArray) return [];
    let outputArray = [];
    for(let arrayIndex = 0; arrayIndex < inputArray.length; arrayIndex++){
        if(!inputArray[arrayIndex]){ //checks if the element of the is not undefined.
            continue;
        } else if(callBackFunction(inputArray[arrayIndex])){  //appends to output if the callBackFunction returns true.
            outputArray.push(inputArray[arrayIndex]);
        }
    }
    return outputArray;
}

module.exports = {filter};