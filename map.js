function map(inputArray, callBacks){
    if(!inputArray) return [];
    let outputArr = [];
    for(let arrIndex = 0; arrIndex < inputArray.length; arrIndex++){
        //if the array containes undefined elements 
        //then it returns undefined in the corresponding index.
        if(!inputArray[arrIndex]){
            outputArr.push(undefined);
        } else {
            outputArr.push(callBacks(inputArray[arrIndex]));
        }
    }
    return outputArr;
}

module.exports = {map};