const {each} = require('../each.js');

const numbers = [10, 9, 8, 7];
//function updates array elements with the sum of array value and index
function myFunction(item, index, arr) {  
    arr[index] = item + index;
  }

each(numbers, myFunction);
console.log(numbers);