const {reduce} = require('../reduce.js');

let numbers = [1];
//callBackFunction that updates the accumlator value in reduce function.
function myFunction(total, num) {
  total--;
  //console.log(total, num);
  return total + num;
}

console.log(reduce(numbers, myFunction));
console.log(numbers.reduce(myFunction));