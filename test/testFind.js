const { find } = require('../find.js');

let numbers = [3, 5, 7, 2, 4, 8];
//function return true if value parameter is even number.
function myFunction (value){
    if(value % 2 == 0) return true;
    return false;
}
console.log(find(numbers, myFunction));