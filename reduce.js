function reduce(inputArray, callBackFunction, currentValue){
    let startIndex = 0;
    if(currentValue == undefined){
        if(inputArray.length == 0){
            return undefined;
        } else{
            currentValue = inputArray[0];
            startIndex++;
        }
    }
    if(!inputArray || !callBackFunction) return undefined;
    for(let currentIndex = startIndex; currentIndex < inputArray.length; currentIndex++){
        //if an element is undefined then function stopes and returns undefined.
        if(!inputArray[currentIndex]){
            return ;
        } else {
            //accumlator(currentValue) variable is updates by the return of callBackFunction for every iteration. 
            currentValue = callBackFunction(currentValue, inputArray[currentIndex], currentIndex, inputArray);
        }
    }
    return currentValue
}

module.exports = {reduce};